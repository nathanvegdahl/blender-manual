
************
Introduction
************

:ref:`Tweak <tool-select-tweak>`
   Selects or moves from the same tool. Click and drag to create a "box" selection.

:ref:`Blade <tool-blade>`
   Create a cut along the strip.
