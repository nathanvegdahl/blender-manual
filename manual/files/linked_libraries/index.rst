.. _bpy.types.Library:

####################
  Linked Libraries
####################

.. toctree::
   :maxdepth: 2

   link_append.rst
   library_overrides.rst
