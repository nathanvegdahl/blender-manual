
#################
  Tool Settings
#################

.. toctree::
   :maxdepth: 2

   texture_slots.rst
   /sculpt_paint/brush/index.rst
   brush_settings.rst
   mask.rst
   symmetry.rst
   options.rst
   tiling.rst
