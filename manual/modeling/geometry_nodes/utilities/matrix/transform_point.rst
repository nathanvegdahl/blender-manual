.. index:: Geometry Nodes; Transform Point
.. _bpy.types.FunctionNodeTransformPoint:

********************
Transform Point Node
********************

.. figure:: /images/node-types_FunctionNodeTransformPoint.webp
   :align: right
   :alt: Transform Point node.

The *Transform Point* node applies a :term:`Transformation Matrix` to a position vector.


Inputs
======

Vector
   The position of a point to transform.
Transformation
   The transformation matrix.


Properties
==========

This node has no properties.


Outputs
=======

Vector
   The transformed point.
