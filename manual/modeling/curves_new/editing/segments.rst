
********
Segments
********

.. _bpy.ops.curves.subdivide:

Subdivide
=========

.. reference::

   :Mode:      Edit Mode
   :Menu:      :menuselection:`Curves --> Segments --> Subdivide`

Curve subdivision simply subdivides all selected segments by adding one or
more control points between the selected segments.

Number of Cuts
   The number of subdivisions to perform.


.. _bpy.ops.curves.switch_direction:

Switch Direction
================

.. reference::

   :Mode:      Edit Mode
   :Menu:      :menuselection:`Curves --> Segments --> Switch Direction`

This tool will "reverse" the direction of any curve with at least one selected element
(i.e. the start point will become the end one, and *vice versa*).
This is mainly useful when using a curve as path, or using the bevel and taper options.
