.. _extensions-licenses:
.. index:: Licenses

******************
Extension Licenses
******************

The `Blender Extensions Platform <https://extensions.blender.org>`__
only supports free and open source extensions compliant with the
`Blender's license <https://www.blender.org/about/license>`__:

- For add-ons, the required license is
  `GNU General Public License v3.0 or later <https://spdx.org/licenses/GPL-3.0-or-later.html>`__.
- For themes, the recommended license is
  `GNU General Public License v3.0 or later <https://spdx.org/licenses/GPL-3.0-or-later.html>`__,
  but any `GPL-compatible license <https://www.gnu.org/licenses/license-list.en.html#GPLCompatibleLicenses>`__
  is supported.
- For assets used in add-ons, the required license is
  `Public Domain (CC0) <https://spdx.org/licenses/CC0-1.0.html>`__.
